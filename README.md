# Validator Service for NestJS 

Usage:

```typescript
import * as path from 'path'
import { Module } from '@nest/common'
import { ValidatorModule, loadSchemas } from '@nestling/validator'
import { mongoDbKeywords } from '@nestling/validator-keywords-mongodb'

const schemaDir = path.resolve(__dirname, '../../schemas')

const schemas = loadSchemas(schemaDir)
// augment schemas with custom keywords for validation

@Module({
  imports: [
    ValidatorModule.forRoot({
      schemas,
      keywords: {
        ...mongoDbKeywords
      }
    }),
    ...
  ]
})
export class ApplicationModule {}
```

```typescript
await this.validatorService.validate('login', user)
```
