import { ValidateFunction } from 'ajv'

export type Schemas = Schema[]

export interface Validator {
  keyword: string
  async?: boolean
  type: string
  validate: ValidateFunction
}

export interface Constructor<T> {
  new (...args: any[]): T
}

export type ValidatorType = Constructor<Validator>

export type Schema = object
