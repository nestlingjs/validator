import * as glob from 'glob'
import * as path from 'path'
import { readSchema } from './readSchema'

export function loadSchemas (...args: any[]) {
  const dir = path.resolve(...args)
  const files = glob.sync(
    path.resolve(
      dir,
      '*.json'
    )
  )

  return files.map(readSchema)
}
