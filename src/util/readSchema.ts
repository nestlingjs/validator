import * as fs from 'fs'

export function readSchema (file: string) {
  return JSON.parse(fs.readFileSync(
    file,
    'utf8'
  ))
}
