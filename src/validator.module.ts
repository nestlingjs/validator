import {
  DynamicModule,
  Global,
  Module
} from '@nestjs/common'

import {
  ValidatorService,
  ValidatorServiceType
} from './validator.service'
import {
  Schema,
  ValidatorType
} from './types'

export type ValidatorOptions = {
  schemas: Schema[]
  metaSchemas?: Schema[]
  validators: ValidatorType[]
}

export interface ValidatorProvider {
  provide: ValidatorServiceType,
  useFactory: (...injections: any[]) => ValidatorService
  inject: ValidatorType[]
}

export type ValidatorForRootOptions = ValidatorOptions & { inject?: any[] }

export function createValidatorProvider (options: ValidatorOptions): ValidatorProvider {
  return {
    provide: ValidatorService,
    useFactory: (...injections: any[]) => {
      const validatorService = new ValidatorService()

      validatorService.configure(
        options.schemas,
        injections,
        options.metaSchemas
      )

      return validatorService
    },
    inject: options.validators
  }
}
@Global()
@Module({})
export class ValidatorModule {
  static forRoot (options: ValidatorForRootOptions): DynamicModule {
    const inject = options.inject || []
    const provider = createValidatorProvider(options)
    return {
      module: ValidatorModule,
      providers: [
        provider,
        ...options.validators,
        ...inject
      ],
      exports: [provider]
    }
  }
}
