import { Injectable } from '@nestjs/common'
import * as Ajv from 'ajv'
import * as setupAsync from 'ajv-async'
import {
  Constructor,
  Schema,
  Schemas,
  Validator
} from './types'

const draft6 = require('ajv/lib/refs/json-schema-draft-06.json')

export type ValidatorServiceType = Constructor<ValidatorService>

@Injectable()
export class ValidatorService {
  schemas: Schemas = []
  _validators: Map<string, Ajv.ValidateFunction> = new Map()
  configure (
    schemas: Schemas,
    validators: Validator[],
    metaSchemas: Schemas = []
  ) {
    this.schemas = schemas

    const ajvSync = new Ajv()

    ajvSync.addMetaSchema(draft6)

    metaSchemas.forEach((schema: Schema) => {
      ajvSync.addMetaSchema(schema)
    })

    if (validators.length) {
      validators.forEach((validator: Validator) => {
        if (!validator.keyword) {
          throw Error('Validator keyword required.')
        }
        if (!validator.validate) {
          throw Error('Expected a validate method.')
        }

        validator.validate = validator.validate.bind(validator)

        ajvSync.addKeyword(
          validator.keyword,
          validator
        )
      })
    }

    const ajv = setupAsync(ajvSync)

    this.schemas.forEach((schema: Schema) => {
      const compiled = ajv.compile(schema)

      this._validators.set((schema as any).$id, compiled)
    })
  }

  async validate (schema: string, data: any) {
    if (this._validators.has(`${schema}.json`)) {
      return (this._validators.get(`${schema}.json`) as Ajv.ValidateFunction)(data)
    }

    throw Error('Cannot find schema: ${schema}.json')
  }
}
